import time
import constants
import random
import codecs
import json


# --- Attendre que l'utilisateur appuie sur entrée
def waitInput(ms):
    if ms[0] != "0":
        print("Le meilleur score date du", ms[0][:10], ":", ms[1], "secondes.")
    input("Appuie sur Entrée pour une nouvelle partie !")


# --- Récupérer le nombre tapé par l'utilisateur
def getInput():
    # Boucle infinie
    while True:
        try:
            # Demander un nombre, le retourner et ainsi finir la boucle
            taille = int(input("Trouve le nombre ! (entre 0 et 200) "))
            return taille
        # Si le résultat n'est pas un nombre...
        except ValueError:
            # Montrer une erreur et continuer la boucle
            print("Nan mais faut que ce soit un chiffre hein...")
            continue


# --- Définir le (nouveau ou original) juste prix
def getJustePrix():
    # Retourner un nombre au hasard entre 0 et 200 (leRange = (0,200) dans constants.py)
    return int(random.randrange(constants.RANGE[0], constants.RANGE[1]))


# --- Demander l'heure
def getTime():
    return time.time()


# --- Vérifier si le temps est écoulé
# ------ argument: (original time, dès que l'utilisateur a appuyé sur Entrée)
def checkTime(oT):
    if time.time() - oT > constants.MAXTIME:
        return False
    return True


# --- Calculer et afficher le résultat du temps passé à trouver le juste prix
# ------ argument: (original time, dès que l'utilisateur a appuyé sur Entrée)
def showTimer(tT, ms):
    if tT > constants.MAXTIME:
        print("Par contre tu as mis trop de temps...")
    else:
        if tT < ms:
            print("Wow, tu as battu le record ! Nouveau record :", "{:.2f}".format(tT), "secondes")
        else:
            print("Ca t'a pris", "{:.2f}".format(tT), "secondes pour trouver le bon nombre.")
    # Petite pause d'une seconde avant de relancer un round...
    time.sleep(1)


def getResult(i, jP):
    if i < jP:
        return 0
    elif i > jP:
        return 1
    return 2


# --- Afficher si le nombre de l'utilisateur est trop bas, haut ou exact
# ------ arguments: (nombre, juste prix)
def showResult(i, jP):
    if i == 0:
        print("Trop bas!")
    elif i == 1:
        print("Trop haut!")
    else:
        print("Bravo ! Le juste prix était :", jP)


def writeResultText(oT, T, sr):
    # Créer une liste pour les en-têtes
    # en_tete = "Résultat, Temps écoulé, Nombre d' essais\n"
    temps = "{:.2f}".format(time.time() - oT)
    tentatives = str(T)
    if sr == 2:
        resultat = "Gagné"
    else:
        resultat = "Perdu"
    ligne = "%s, %s s, %s essais\n#######################\n" % (resultat, temps, tentatives)
    # Créer un nouveau fichier pour écrire dans le fichier appelé « data.csv »
    with codecs.open('resultats.txt', 'a', 'UTF-8') as fichier_txt:
        # Créer un objet writer (écriture) avec ce fichier
        # fichier_txt.write(en_tete)
        fichier_txt.write(ligne)
        # Parcourir les titres et descriptions - zip permet d' itérer sur deux listes ou plus à la fois
        # Créer une nouvelle ligne avec le titre et la description à ce moment de la boucle
        fichier_txt.close()


def writeJson(won, tT, T):
    with open("resultats.json", "r") as jsonFile:
        try:
            data = json.load(jsonFile)
        except json.JSONDecodeError:
            data = {}
    with open("resultats.json", "w") as jsonFile:
        data[time.ctime(getTime())] = {
            "Gagné": won,
            "Durée": "{:.2f}".format(tT),
            "Essais": T,
        }
        json.dump(data, jsonFile, indent=4, ensure_ascii=False)


def getMeilleurScore():
    ms = ["0", constants.MAXTIME]
    with open("resultats.json", "r") as jsonFile:
        try:
            data = json.load(jsonFile)
            for d in data:
                if float(data[d]["Durée"]) < ms[1]:
                    ms = [str(d), float(data[d]["Durée"])]
        except json.JSONDecodeError:
            pass
        return ms


def writeEndLoop(ot, T, sr, w, tT):
    writeResultText(ot, T, sr)
    writeJson(w, tT, T)


def mainLoop():
    # Boucle infinie
    while True:
        # On initialise/remet à zéro toutes les variables
        justePrix = getJustePrix()
        inpt = 0
        tentatives = 0
        sr = 0
        meilleurScore = getMeilleurScore()
        waitInput(meilleurScore)
        originalTime = getTime()
        # On demande à l'utilisateur de taper un nombre jusqu'à ce qu'il soit bon, en boucle
        while inpt != justePrix:
            inpt = getInput()
            timeTaken = getTime() - originalTime
            if not checkTime(originalTime):
                writeEndLoop(originalTime, tentatives, 0, False, timeTaken)
                print("Désolé, temps écoulé...")
                break
            sr = getResult(inpt, justePrix)
            showResult(sr, justePrix)
            tentatives += 1
            if sr == 2:
                writeEndLoop(originalTime, tentatives, sr, checkTime(originalTime), timeTaken)
                showTimer(timeTaken, meilleurScore[1])
                break

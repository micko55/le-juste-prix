# On importe tout (*) de defs : 
# Cette façon de l'écrire évite d'avoir à taper defs. à chaque fois qu'on appelle une fonction
# On pourrait simplement faire import defs, mais il faudrait utiliser defs.getJustePrix(), defs.getTime(), etc.
import defs

if __name__ == "__main__":
    defs.mainLoop()
